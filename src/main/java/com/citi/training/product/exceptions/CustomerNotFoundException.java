/**
 * 
 */
package com.citi.training.product.exceptions;

/**
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class CustomerNotFoundException extends RuntimeException {
	
	public CustomerNotFoundException(String message) {
		super(message);
		
	}
	
}
