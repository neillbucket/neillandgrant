/**
 * 
 */
package com.citi.training.product.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.product.dao.CustomerDao;
import com.citi.training.product.model.Customer;



/**
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {


	// logging

	// Second half says say that all these logs are from ProductController class
	// (Tag them as that)
	private static org.slf4j.Logger LOG = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	private CustomerDao customerDao;

	@RequestMapping(method = RequestMethod.GET) // request will come to this function as its .GET
	public List<Customer> getAll() {
		LOG.info("getAll was called");
		return customerDao.getAllCustomers();
	}

	@RequestMapping(method = RequestMethod.POST, produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE, consumes = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Customer> addCustomer(@RequestBody Customer customer) {
		// productRepository.saveProduct(new Product(3, "Mop", 9.99));
		LOG.info("AddProduct was called");
		// LOG.debug("This is a debug message");
		customerDao.saveCustomer(customer);

		return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	public Customer getCustomer(@PathVariable int id) {
		LOG.info("GetCustomer was called");
		return customerDao.getCustomer(id);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE) // , consumes =
																	// org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteCustomer(@PathVariable int id) {
		LOG.info("delete was called, id: " + id);
		customerDao.deleteCustomer(id);;
	}
}
