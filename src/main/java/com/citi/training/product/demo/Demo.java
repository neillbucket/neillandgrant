/**
 * 
 */
package com.citi.training.product.demo;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.citi.training.product.dao.CustomerDao;
import com.citi.training.product.model.Customer;


@Component
public class Demo implements ApplicationRunner {

    @Autowired
    private CustomerDao customerDao;

    public void run(ApplicationArguments appArgs) {
        System.out.println("Creating some customers");
        String[] customerNames = {"Beans", "Jam", "Ham"};
        Random random = new Random();

        for(int i=0; i<customerNames.length; ++i) {
            Customer thisCustomer = new Customer(i,
                                              customerNames[i],
                                              "address");

            System.out.println("Created Customer: " + thisCustomer);

            customerDao.saveCustomer(thisCustomer);
        }

        System.out.println("\nAll Customers:");
        for(Customer customer: customerDao.getAllCustomers()) {
            System.out.println(customer);
        }
  
        System.out.println("Demo Finished - Exiting");
    }

    private double roundTwoPlaces(double value) {
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
