package com.citi.training.product.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.citi.training.product.exceptions.CustomerNotFoundException;
import com.citi.training.product.model.Customer;

@Component
public class InMemoryCustomerDao implements CustomerDao {

	private Map<Integer, Customer> allCustomers = new HashMap<Integer, Customer>();

	public void saveCustomer(Customer customer) {
		allCustomers.put(customer.getId(), customer);
	}

	public Customer getCustomer(int id) {

		if (allCustomers.get(id) == null) {
			throw new CustomerNotFoundException("Customer not Found. No Customer added.");
		} else {
			return allCustomers.get(id);
		}
	}

	public List<Customer> getAllCustomers() {
		return new ArrayList<Customer>(allCustomers.values());
	}

	public void deleteCustomer(int id) {
		Customer removeCustomer = allCustomers.remove(id);
		
		if(removeCustomer == null) {
			throw new CustomerNotFoundException("Customer does not exist");
		} 
	}


	

}

