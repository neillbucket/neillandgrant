package com.citi.training.product.dao;

import java.util.List;

import com.citi.training.product.model.Customer;

public interface CustomerDao {
	
	    void saveCustomer(Customer customer);

	    Customer getCustomer(int id);

	    List<Customer> getAllCustomers();
	    
	    void deleteCustomer(int id);

}
