package com.citi.training.product.model;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.rules.TestName;

public class CustomerTest {
	/**
	 * Test information
	 */
	int testID = 1;
	String testName = "testName";
	String testAddress = "addressName";
	
	
	/**
	 * Test for ID getters and setters
	 */
	@Test
	public void getSetID() {
		Customer testCustomer = new Customer();
		testCustomer.setId(testID);
		
		assertEquals(testCustomer.getId(), testID);
	}
	
	/**
	 * Test for name getters and setters
	 */
	@Test
	public void getSetName() {
		Customer testCustomer = new Customer();
		testCustomer.setName(testName);
		
		assertEquals(testCustomer.getName(), testName);
	}
	
	/**
	 * Test for address getters and setters
	 */
	@Test
	public void getSetAddress() {
		Customer testCustomer = new Customer();
		testCustomer.setAddress(testAddress);
		
		assertEquals(testCustomer.getAddress(), testAddress);
	}
}
